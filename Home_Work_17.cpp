﻿#include <iostream>

class Example {
private:
    int a;
    int b;
public:
    int getA() {
        return a;
    }
    int getB() {
        return b;
    }
    void set(int numA, int numB) {
        a = numA;
        b = numB;
    }
};

class Vector {
private:
    double x = 0;
    double y = 0;
    double z = 0;
public:
    Vector() : x(1), y(3), z(5) 
    {}
    void show() {
        std::cout << (x * x + y * y + z * z);
    }

};

int main()
{
    Example temp;
    temp.set(8, 5);
    std::cout << temp.getA() << " " << temp.getB();

    std::cout << std::endl;

    Vector vec;
    vec.show();
}